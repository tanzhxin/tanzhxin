---
title: "Emacs和systemd"
date: 2020-02-20T16:28:14+08:00
draft: false
---

用户服务：  ~/.config/systemd/user/emacs
systemctl --user enable emacs
systemctl --user start emacs

内容如下：

```
[Unit]
Description=Emacs daemon
After=syslog.target network.target
 
[Service]
Type=forking
ExecStart=/usr/bin/emacs --daemon
ExecStop=/usr/bin/emacsclient --eval "(progn (setq kill-emacs-hook 'nil) (kill-emacs))"
Restart=always
Environment=SSH_AUTH_SOCK=/run/user/1000/keyring/ssh GPG_AGENT_INFO=/run/user/1000/keyring/gpg:0:1
TimeoutStartSec=0
 
[Install]
WantedBy=multi-user.target

```


ok！



---
title: "XRDP远程登录错误"
date: 2020-03-09T18:12:58+08:00
draft: false
---


xRDP远程登陆错误color-manager-profile


xdmcp远程登录centos7服务器

遇到color manager profile 错误，需要输入密码，猜测为X服务器问题，搜索结果证实确实如此

https://unix.stackexchange.com/questions/417906/authentication-is-required-to-create-a-color-profile

修复代码如下：

创建文件 /etc/polkit-1/localauthority/50-local.d/color.pkla



```
[Allow colord for all users]
Identity=unix-user:*
Action=org.freedesktop.color-manager.create-device;org.freedesktop.color-manager.create-profile;org.freedesktop.color-manager.delete-device;org.freedesktop.color-manager.delete-profile;org.freedesktop.color-manager.modify-device;org.freedesktop.color-manager.modify-profile
ResultAny=yes
ResultInactive=yes
ResultActive=yes


```



创建规则文件  /etc/polkit-1/rules.d/


```

polkit.addRule(function(action, subject) {
   if ((action.id == "org.freedesktop.color-manager.create-device" ||
        action.id == "org.freedesktop.color-manager.create-profile" ||
        action.id == "org.freedesktop.color-manager.delete-device" ||
        action.id == "org.freedesktop.color-manager.delete-profile" ||
        action.id == "org.freedesktop.color-manager.modify-device" ||
        action.id == "org.freedesktop.color-manager.modify-profile") &&
        subject.isInGroup("ATTENTION")) {
                return polkit.Result.YES;
	}
 });



```



搞定。



---
title: "Imagemagick用法"
date: 2019-01-22T16:55:23+08:00
draft: false
---

imagemagick是一个很高效的命令行图片处理工具，可以结合shell编程实现批量处理，加快处理效率。一般我用来批量处理的工作有：批量改变大小、批量改变添加水印等。

{% asset_img imagemagick.png 白胡子老头 %}


imagemagick用法：

命令格式如下：

convert <输入图片名> [+/-命令和命令的参数]* <输出图片名>

常用的命令有：

-crop 宽x高+起点横坐标+起点纵坐标：裁剪图[]

-resize 宽x高[!]：改变尺寸，如果使用惊叹号，表示不保留视觉比例，强行改变尺寸匹配给定的宽和高

-colors 颜色数：设定图片采用的颜色数，如果是生成png或gif图片应指定这个参数

-quality 质量：设定jpeg图片输出质量，推荐采用80，此命令仅用于输出格式是jpg的情况，不应省略，省略的话默认质量是95，生成图片过大+profile "*"：图片中不存储附加信息，必须使用，否则生成图片过大

1、看图片： display命令；如果想看这个文件夹下的所有图片，display *即可。space是翻页；

2、调整图片大小： convert -resize xxx*xxx a.jpg b.jpg 这是用解析度为参数；其实更方便的是用相对大小， convert -resize 25% a.jpg b.jpg。这个命令支持批处理，很赞；

3、改变图片格式： convert a.jpg b.bmp等等，随便写，靠后缀来识别。当图片需要跨平台时，这个功能显得尤为实用；

4、批量生成缩略图： mogrify -sample 80*60 *.jpg 不过这个命令会覆盖原有的文件，操作前需要进行原始文件的备份；

5、图片加框（非常实用）：convert -border 60*60 “#000000″ a.jpg b.jpg，这里，60＊60是表示边框的宽度，第一个是纵边框的宽度，第二个是横边框的宽度，＃000000是RGB格式的边框色彩，通常不是黑色就是 白色，无所谓了；

6、在图片上加上文字：对于图片分来来说，很是赞，convert -fill green -pointsize 40 -draw ‘text 10,50 “charry.org”‘ a.jpg b.jpg。表示在距离图片左上角10＊50的位置处，用绿色的文字写下charry.org，如果要指定别的字体，可以用 -font参数；

7、高斯模糊：convert -blur 80 a.jpg b.jpg 参数还可以写成 -blur 80*5 后面的5表示sigma值；

8、上下翻转： convert -flip a.jpg b.jpg

9、左右翻转： convert -flop a.jpg b.jpg

10、反色，形成底片的样子 convert -negate a.jpg b.jpg

11、变成黑白色：convert -monochrome a.jpg b.jpg 是黑白色，不是灰度图

12、油画的效果，有的时候很有感觉 convert -paint 4 a.jpg b.jpg

13、旋转一定角度： convert -rotate 30 a.jpg b.jpg

14、炭笔的效果，和油画的使用方法一样，把paint 换成 charcoal

15、毛玻璃效果，同上，参数为 -spread



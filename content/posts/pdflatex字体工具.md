---
title: "Pdflatex字体工具"
date: 2019-11-26T11:07:27+08:00
draft: false
---


pdflatex需要特殊的字体以及配置文件，当然手工能处理
不过这个过程过于复杂，最后我在smth上找到了一个bash脚本，发现能够大大减少工作量，
而且可以在linux下生成字体文件后再下载安装到windows机器的texmf-localm目录，然后刷新字体就ok
目前还有点问题就是有些字体无法正常通过以上流程，比如simsunb.ttf和msyh.ttf
可能要考虑用fontforge工具手工处理



首先是脚本文件

```

#!/bin/sh
#
# By Edward G.J. Lee <edt1023@info.sayya.org>
# Modified by L.M. Wang <longminwang@gmail.com>
# This code is Public Domain.
# You will need ttf2pt1/t1utils to use this script.
#
# Changelog:
#
# 01-11-23 產生之子目錄加上字型名稱，以資識別。
#          Use subdirectory.
# 02-06-10 加入產生斜體字的設定(配合 fancyhdr package 所需)。
#          產生 (dvips & pdftex)map 檔。
#          產生 dvipdfm-cjk 所需的 cid.map 內容。
#          加入一些必要的測試函數。
#          slant-tfm, and create map files.
#          fix wrong path in check_tools().
# 02-06-12 fix (dvips)map(wrong subfonts' name).
#          use printf in create_type1(), comment from KhoGuan
#          <khoguan.bbs@bbs.sayya.org>
# 02-06-15 fix (dvios)map typo
#          use NUMLIST list instead number counter of for/whlie loops, thank
#          edwar <edwar.bbs@bbs.sayya.org>
#          merge create_tfm() to create_map()
# 02-06-18 use ttfinfo. add check_enc.
#          自動判斷字型是否為 Unicode font，並自動判斷 big5 或 gb 碼，做不同
#          處理。字型已不必放在工作目錄下，可以處理 path。
#          Oops, bug in create_map().
# 02-06-19 增加選擇 CJK 所有使用的字型名稱。謝謝
#          goldencat <goldencat@ezdelivery.net>
# 03-06-21 fixed create_cidmap() to fit dvipdfmx(former dvipdfm-cjk).
# 06-09-17 增加 GBK, UTF8 支持
#	   将文件复制到 $TEXMF, 默认为 $HOME/texmf
#
# Functions:
# check_tools()    : check if ttf2pt1 package exist.
# check_ttf()      : check if file is TTF(C) and what the encoding is.
# check_enc()      : assign encoding, big5 and gb2312 only so far.
# create_type1()   : ttf2pt1
# create_tfm()     : merge to create_map(), we need not too many for loops.
# create_map()     : *.tfm for TeX and map files for dvips/pdftex
# create_cidmap()  : cid.map for dvipdfm-cjk.
# create_cjkfd()   : .fd file for CJK.
# create_pk()      : not yet.
# mkfont_help()    : not yet.
# select_func()    : not yet, may be will inside the main()
# use via ttfm     : not yet.
# choice_cjkname   : font name for CJK : not yet.
# install()        : 03-06-23
# write in C       : noooot yet, will include ttf2pt1/oto/showttf codes.
# GUI(gtk+)        : noooooooooooot yet.
#
# $Id: mkfont.sh,v 1.4 2004/07/16 09:54:47 edt1023 Exp $
#

FULLNAME=$1
TTF2PT1=`which ttf2pt1`
DATE=`date`
# TEXMF=$HOME/texmf
TEXMF=/usr/share/texlive/texmf-local/

if [ $# -eq 0 ]
then
  echo
  echo "Usage: `basename $0` your.ttf font_name"
  echo
  exit
elif [ $# -eq 1 ]
then
  FNAME=`basename $FULLNAME`
  eval `echo $FNAME | awk -F. '{printf "FHEAD=%s;FTAIL=%s",\$1,\$2}'`
  FONTNAME=$FHEAD
elif [ $# -eq 2 ]
then
  FNAME=`basename $FULLNAME`
  eval `echo $FNAME | awk -F. '{printf "FHEAD=%s;FTAIL=%s",\$1,\$2}'`
  FONTNAME=$2
elif [ $# -eq 3 ]
then
  FNAME=`basename $FULLNAME`
  eval `echo $FNAME | awk -F. '{printf "FHEAD=%s;FTAIL=%s",\$1,\$2}'`
  FHEAD=$2
  FONTNAME=$3
else
  echo
  echo "Usage: `basename $0` your.ttf subfont_name font_name"
  echo
  exit
fi

check_tools()
{
  if [ ! -x "$TTF2PT1" ]
  then
    echo
    echo "You need ttf2pt1 to use this script."
    echo
    exit
  fi
}

check_ttf()
{
  if [ ! -f "$FULLNAME" ]
  then
    echo
    echo "File doesn't exist!"
    echo
    exit
  fi
    
  if [ "$FTAIL" != "ttf" ] && \
     [ "$FTAIL" != "TTF" ] && \
     [ "$FTAIL" != "ttc" ] && \
     [ "$FTAIL" != "TTC" ]
  then
    echo
    echo "File is not a TrueType font."
    echo
    exit
  fi
}

check_enc()
{
  if [ -d /usr/local/share/ttf2pt1/maps ]
  then
    MAPDIR=/usr/local/share/ttf2pt1/maps
    elif [ -d /usr/share/ttf2pt1/maps ]
    then
      MAPDIR=/usr/share/ttf2pt1/maps
    elif [ -d $HOME/share/ttf2pt1/maps ]
    then
      MAPDIR=$HOME/share/ttf2pt1/maps
  else
    echo
    echo "Where is your ttf2pt1 Chinese map files?"
    echo
    exit
  fi

  echo
  echo " Which Chinese TTF?"
  echo " 1. Big5  2. Gb2312  3. GBK  4. Unicode"
  echo " choice 1, 2, 3 or 4."
  echo
  # read CHOICE
  CHOICE="4"
  if [ "$CHOICE" = "1" ]
  then
    NUMLIST=`awk 'BEGIN{ n=1; while(n<56){printf "%02d\n",n; n++}}'`
    MAP=${MAPDIR}/cubig5.map
    PSENC1=Big5
    PSENC2=ETen-B5
    CJKENC=00
    FD=${TEXMF}/tex/latex/CJK/Bg5
  elif [ "$CHOICE" = "2" ]
  then
    NUMLIST=`awk 'BEGIN{ n=1; while(n<36){printf "%02d\n",n; n++}}'`
    MAP=${MAPDIR}/cugb.map
    PSENC1=EUC
    PSENC2=GB-EUC
    CJKENC=10
    FD=${TEXMF}/tex/latex/CJK/GB
  elif [ "$CHOICE" = "3" ]
  then
    NUMLIST=`awk 'BEGIN{ n=1; while(n<94){printf "%02d\n",n; n++}}'`
    MAP=${MAPDIR}/cugbk.map
    PSENC1=UGBK
    PSENC2=UniGB-UCS2
    CJKENC=19
    FD=${TEXMF}/tex/latex/CJK/GB
  else
    NUMLIST=`awk 'BEGIN{ n=1; while(n<256){printf "%02x\n",n; n++}}'`
    MAP=${MAPDIR}/unicode-sample.map
    PSENC1=Unicode
    PSENC2=Identity
    CJKENC=70
    FD=${TEXMF}/tex/latex/CJK/UTF8
  fi
}

create_type1()
{
  echo
  echo "Now create *.pfb and *.enc files, wait... "
  echo
  for i in $NUMLIST
  do
    #ttf2pt1 -GE -pttf -Ohub -W0 -L $MAP+$i $FULLNAME ${FHEAD}$i
    ttf2pt1 -GAE -pttf -OHUBs -W0 -l plane+pid=3,eid=1,0x$i $FULLNAME ${FHEAD}$i
  done

# avoid dvips t1part module bugs.
  perl -pi -e 's/_/Z/g' *.t1a *.afm

  for ps in *.t1a
  do
    t1asm -b $ps > ${ps%.t1a}.pfb
  done
}

create_map()
{
MAPFILE=t1-${FHEAD}.map
ENCMAP=ttf-${FHEAD}.map
  echo
  echo "Create *.tfm and (dvips)map file, wait..."
  echo
  cat > $MAPFILE << EndOfFile
% This is map file for dvips/dvipdfm[x] and LaTeX CJK package.
% Created by Edward G.J. Lee <edt1023@info.sayya.org>
% $DATE
EndOfFile
  cat > $ENCMAP << EndOfFile
% This is map file for PDFLaTeX and LaTeX CJK package to embed TTF.
% Created by Edward G.J. Lee <edt1023@info.sayya.org>
% $DATE
EndOfFile
  for i in $NUMLIST
  do
    PSNAME=`awk '/FontName/ {print $2}' ${FHEAD}$i.afm`
    afm2tfm ${FHEAD}$i.afm > /dev/null 2>&1
    afm2tfm ${FHEAD}$i.afm -s .167 ${FHEAD}sl$i.tfm > /dev/null 2>&1
    cat >> $MAPFILE << EndOfFile
${FHEAD}$i $PSNAME <${FHEAD}$i.pfb
${FHEAD}sl$i $PSNAME "0.167 SlantFont" <${FHEAD}$i.pfb
EndOfFile
    cat >> $ENCMAP << EndOfFile
${FHEAD}$i <${FHEAD}$i.enc <${FNAME}
EndOfFile
  done
}

create_cidmap()
{
cat >> cid-x.map << EndOfFile
%
% created by Edward G.J. Lee <edt1023.info.sayya.org>
% $DATE
%
$FHEAD@$PSENC1@ ${PSENC2}-H :0:$FNAME
${FHEAD}s@$PSENC1@ ${PSENC2}-H :0:$FNAME -s .167
$FHEAD@$PSENC1@ ${PSENC2}-H :0:$FNAME,Bold
${FHEAD}s@$PSENC1@ ${PSENC2}-H :0:$FNAME,Bold -s .167
EndOfFile
}

create_cjkfd()
{
cat > c${CJKENC}${FONTNAME}.fd << EndOfFile
% This is c${CJKENC}${FONTNAME}.fd for CJK package.
% created by Edward G.J. Lee <edt1023@info.sayya.org>
% $DATE
%
\ProvidesFile{c${CJKENC}${FONTNAME}.fd}[\filedate\space\fileversion]
\DeclareFontFamily{C${CJKENC}}{$FONTNAME}{\hyphenchar \font\m@ne}
\DeclareFontShape{C${CJKENC}}{$FONTNAME}{m}{n}{<-> CJK * $FHEAD}{}
\DeclareFontShape{C${CJKENC}}{$FONTNAME}{m}{sl}{<-> CJK * ${FHEAD}sl}{}
\DeclareFontShape{C${CJKENC}}{$FONTNAME}{m}{it}{<-> CJKssub * $FONTNAME/m/sl}{}
\DeclareFontShape{C${CJKENC}}{$FONTNAME}{bx}{n}{<-> CJKb * $FHEAD}{\CJKbold}
\DeclareFontShape{C${CJKENC}}{$FONTNAME}{bx}{sl}{<-> CJKb * ${FHEAD}s}{\CJKbold}
\DeclareFontShape{C${CJKENC}}{$FONTNAME}{bx}{it}{<-> CJKssub * $FONTNAME/bx/sl}{\CJKbold}
\endinput
EndOfFile
}

# main()
check_tools
check_ttf
check_enc
create_type1
create_map
create_cidmap
create_cjkfd

AFM=${TEXMF}/fonts/afm/$FHEAD
TFM=${TEXMF}/fonts/tfm/$FHEAD
PFB=${TEXMF}/fonts/type1/$FHEAD
ENC=${TEXMF}/fonts/enc/$FHEAD
MAPDIR=${TEXMF}/fonts/map
rm -f *.t1a
mkdir -p $AFM $TFM $PFB $ENC
mv -f *.enc $ENC
mv -f *.afm $AFM
mv -f *.tfm $TFM
mv -f *.pfb $PFB
mkdir -p $MAPDIR/{dvips,pdftex,dvipdfm}
mv -f $MAPFILE $MAPDIR/dvips
mv -f $ENCMAP $MAPDIR/pdftex
mv -f cid-x.map $MAPDIR/dvipdfm
mkdir -p $FD
mv -f c${CJKENC}${FONTNAME}.fd $FD
echo
echo "OK, all done. :-)"
echo
echo "t1-${FHEAD}.map is for dvips and Type1 Chinese fonts."
echo "ttf-${FHEAD}-enc.map is for pdftex to embed Chinese TTF."
echo "cid-x.map is for dvipdfmx. These files have been copied to"
echo "$TEXMF. Do texhash (or mktexlsr) and updmap --enable"
echo "Map=t1-${FHEAD}.map (ttf-${FHEAD}.map) or updmap-sys" 
echo "--enable Map=t1-${FHEAD}.map (ttf-${FHEAD}.map)."
echo "Good luck.:)"
echo
mktexlsr
updmap-sys --enable Map=$MAPFILE
updmap-sys --enable Map=$MAPFILE
updmap-sys --enable Map=$MA
updmap-sys --enable Map=$ENCMAP
updmap-sys --enable Map=$ENCMAP
updmap-sys --enable Map=$ENCMAP

```

我对文件做了一点调整，就是texmf目录
然后把linux机器的texlive刷新也写进了bash脚本

使用的时候把ttf字体文件改成小写，然后直接运行
```
mkfont.sh simhei.ttf
```

建议还是只用一个参数，避免使用kaishi lishu这些特定的命名，这样以后还可以处理stxinwei.ttf

在latex文件中切换字体就是
```
\CJKfamily{simli} \selectfont
```


然后就切换成了隶书
当然也可以定义一个tex命令

```
\newcommand{\li}{\CJKfamily{simli}}        % 隶书   (Windows自带simli.ttf)
```

然后就是一般使用了

其他一些中文设置也附带放这里

```
\usepackage{mathbbold}
\usepackage{CJK,CJKnumb}
\usepackage{indentfirst}        %首行缩进宏包
\usepackage{latexsym,bm}        % 处理数学公式中和黑斜体的宏包
\usepackage{amsmath,amssymb}    % AMSLaTeX宏包 用来排出更加漂亮的公式
\usepackage{graphicx}
\usepackage{cases}
\usepackage{pifont}
\usepackage{txfonts}

%%%%%%%%%%% CJK下设置中文字体 %%%%%%%%%%%%%
\newcommand{\song}{\CJKfamily{song}}    % 宋体   (Windows自带simsun.ttf)
\newcommand{\fs}{\CJKfamily{fs}}        % 仿宋体 (Windows自带simfs.ttf)
\newcommand{\kai}{\CJKfamily{kai}}      % 楷体   (Windows自带simkai.ttf)
\newcommand{\hei}{\CJKfamily{hei}}      % 黑体   (Windows自带simhei.ttf)
\newcommand{\li}{\CJKfamily{li}}        % 隶书   (Windows自带simli.ttf)

%%%%%%%%%%%  设置字体大小 %%%%%%%%%%%%%
\newcommand{\chuhao}{\fontsize{42pt}{\baselineskip}\selectfont}
\newcommand{\xiaochuhao}{\fontsize{36pt}{\baselineskip}\selectfont}
\newcommand{\yihao}{\fontsize{28pt}{\baselineskip}\selectfont}
\newcommand{\erhao}{\fontsize{21pt}{\baselineskip}\selectfont}
\newcommand{\xiaoerhao}{\fontsize{18pt}{\baselineskip}\selectfont}
\newcommand{\sanhao}{\fontsize{15.75pt}{\baselineskip}\selectfont}
\newcommand{\sihao}{\fontsize{14pt}{\baselineskip}\selectfont}
\newcommand{\xiaosihao}{\fontsize{12pt}{\baselineskip}\selectfont}
\newcommand{\wuhao}{\fontsize{10.5pt}{\baselineskip}\selectfont}
\newcommand{\xiaowuhao}{\fontsize{9pt}{\baselineskip}\selectfont}
\newcommand{\liuhao}{\fontsize{7.875pt}{\baselineskip}\selectfont}
\newcommand{\qihao}{\fontsize{5.25pt}{\baselineskip}\selectfont}

%%%%%%%% 设置版心 %%%%%%%%%%%%%
\setlength{\textwidth}{14cm}
\setlength{\textheight}{20cm}
\setlength{\hoffset}{0cm}
\setlength{\voffset}{0cm}

\setlength{\parindent}{2em}                 % 首行两个汉字的缩进量
\setlength{\parskip}{3pt plus1pt minus1pt} % 段落之间的竖直距离
\renewcommand{\baselinestretch}{1.2}        % 定义行距
\setlength{\abovedisplayskip}{2pt plus1pt minus1pt}     %公式前的距离
\setlength{\belowdisplayskip}{6pt plus1pt minus1pt}     %公式后面的距离
\setlength{\arraycolsep}{2pt}   %在一个array中列之间的空白长度, 因为原来的太宽了

\allowdisplaybreaks[4] % \eqnarray如果很长，影响分栏、换行和分页
                        %（整块挪动，造成页面空白），可以设置成为自动调整模式


\CJKtilde   %用于解决英文字母和汉字的间距问题。例如：变量~$x$~的值。
\renewcommand{\CJKglue}{\hskip 0pt plus 0.08\baselineskip}
            %它于必要时在汉字之间插入一个附加的空隙，以解决行的超长问题。

%\numberwithin{equation}{section}
%================= 一些自定义命令 =============%
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\newcommand{\p}{\partial}
\newcommand{\g}{\gamma}
%=================== End ======================%
\usepackage{fancyhdr}
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0.6pt}

%\addtolength{\parinddent}{2em plus 1em minus 1em]
%===============标题名称中文化 ===================%
\renewcommand\abstractname{\hei 摘\ 要}
\renewcommand\refname{\hei 参考文献}
\renewcommand\figurename{\hei 图}
\renewcommand\tablename{\hei 表}
\newtheorem{dingyi}{\hei 定义~}[section]
\newtheorem{dingli}{\hei 定理~}[section]
\newtheorem{yinli}[dingli]{\hei 引理~}
\newtheorem{tuilun}[dingli]{\hei 推论~}
\newtheorem{mingti}[dingli]{\hei 命题~}

```

注：在印刷出版上，中文字号制与点数制的对照关系如下：
  1770年法国人狄道（F.A.Didot）制定点数制，规定1法寸为72点，即：1点=0.3759毫米。
  狄道点数制在法国、德国、奥地利、比利时、丹麦、匈牙利等国比较流行。
  1886年全美活字铸造协会以派卡（pica）为基准制定派卡点数制，规定1pica=12point（点），即：
  1点=0.013837英寸=0.35146毫米
  20世纪初派卡点数制传入我国，并得到逐步推广。在实用中对常用点数以号数命名而产生了号数制，
  二者换算如下（以pt代表点）：
  初号42pt   小初号36pt    一号28pt     二号21pt    小二号18pt   三号15.75pt
  四号14pt   小四号12pt    五号10.5pt   小五号9pt   六号  7.875pt  七号  5.25pt


以下为xeCJK的设置，对于pdflatex来说仅作参考


```
%%%%% xeCJK下设置中文字体 %%%%%%%%%%%
\setCJKfamilyfont{song}{SimSun}                                 %宋体 song
\newcommand{\song}{\CJKfamily{song}}
\setCJKfamilyfont{xs}{NSimSun}                            %新宋体 xs
\newcommand{\xs}{\CJKfamily{xs}}
\setCJKfamilyfont{fs}{FangSong_GB2312}        %仿宋2312 fs
\newcommand{\fs}{\CJKfamily{fs}}
\setCJKfamilyfont{kai}{KaiTi_GB2312}                        %楷体2312  kai
\newcommand{\kai}{\CJKfamily{kai}}
\setCJKfamilyfont{yh}{Microsoft YaHei}                    %微软雅黑 yh
\newcommand{\yh}{\CJKfamily{yh}}
\setCJKfamilyfont{hei}{SimHei}                                    %黑体  hei
\newcommand{\hei}{\CJKfamily{hei}}
\setCJKfamilyfont{msunicode}{Arial Unicode MS}            %Arial Unicode MS: msunicode
\newcommand{\msunicode}{\CJKfamily{msunicode}}
\setCJKfamilyfont{li}{LiSu}                                            %隶书  li
\newcommand{\li}{\CJKfamily{li}}
\setCJKfamilyfont{yy}{YouYuan}                             %幼圆  yy
\newcommand{\yy}{\CJKfamily{yy}}
\setCJKfamilyfont{xm}{MingLiU}                                        %细明体  xm
\newcommand{\xm}{\CJKfamily{xm}}
\setCJKfamilyfont{xxm}{PMingLiU}                             %新细明体  xxm
\newcommand{\xxm}{\CJKfamily{xxm}}

\setCJKfamilyfont{hwsong}{STSong}                            %华文宋体  hwsong
\newcommand{\hwsong}{\CJKfamily{hwsong}}
\setCJKfamilyfont{hwzs}{STZhongsong}                        %华文中宋  hwzs
\newcommand{\hwzs}{\CJKfamily{hwzs}}
\setCJKfamilyfont{hwfs}{STFangsong}                            %华文仿宋  hwfs
\newcommand{\hwfs}{\CJKfamily{hwfs}}
\setCJKfamilyfont{hwxh}{STXihei}                                %华文细黑  hwxh
\newcommand{\hwxh}{\CJKfamily{hwxh}}
\setCJKfamilyfont{hwl}{STLiti}                                        %华文隶书  hwl
\newcommand{\hwl}{\CJKfamily{hwl}}
\setCJKfamilyfont{hwxw}{STXinwei}                                %华文新魏  hwxw
\newcommand{\hwxw}{\CJKfamily{hwxw}}
\setCJKfamilyfont{hwk}{STKaiti}                                    %华文楷体  hwk
\newcommand{\hwk}{\CJKfamily{hwk}}
\setCJKfamilyfont{hwxk}{STXingkai}                            %华文行楷  hwxk
\newcommand{\hwxk}{\CJKfamily{hwxk}}
\setCJKfamilyfont{hwcy}{STCaiyun}                                 %华文彩云 hwcy
\newcommand{\hwcy}{\CJKfamily{hwcy}}
\setCJKfamilyfont{hwhp}{STHupo}                                 %华文琥珀   hwhp
\newcommand{\hwhp}{\CJKfamily{hwhp}}

\setCJKfamilyfont{fzsong}{Simsun (Founder Extended)}     %方正宋体超大字符集   fzsong
\newcommand{\fzsong}{\CJKfamily{fzsong}}
\setCJKfamilyfont{fzyao}{FZYaoTi}                                    %方正姚体  fzy
\newcommand{\fzyao}{\CJKfamily{fzyao}}
\setCJKfamilyfont{fzshu}{FZShuTi}                                    %方正舒体 fzshu
\newcommand{\fzshu}{\CJKfamily{fzshu}}

\setCJKfamilyfont{asong}{Adobe Song Std}                        %Adobe 宋体  asong
\newcommand{\asong}{\CJKfamily{asong}}
\setCJKfamilyfont{ahei}{Adobe Heiti Std}                            %Adobe 黑体  ahei
\newcommand{\ahei}{\CJKfamily{ahei}}
\setCJKfamilyfont{akai}{Adobe Kaiti Std}                            %Adobe 楷体  akai
\newcommand{\akai}{\CJKfamily{akai}}



```



以下为转载的中文徒手方案设置

页面设置：
```

\documentclass[b5paper, 12pt, onecolumn, oneside]{article}

\geometry{left=2.5cm,right=2.0cm,top=2.5cm,bottom=2.0cm}


```

字号选择：

```
\newcommand{\chuhao}{\fontsize{42pt}{\baselineskip}\selectfont}
\newcommand{\xiaochu}{\fontsize{36pt}{\baselineskip}\selectfont}
\newcommand{\yihao}{\fontsize{28pt}{\baselineskip}\selectfont}
\newcommand{\erhao}{\fontsize{21pt}{\baselineskip}\selectfont}
\newcommand{\xiaoer}{\fontsize{18pt}{\baselineskip}\selectfont}
\newcommand{\sanhao}{\fontsize{16pt}{\baselineskip}\selectfont}
\newcommand{\xiaosan}{\fontsize{15pt}{\baselineskip}\selectfont}
\newcommand{\sihao}{\fontsize{14pt}{\baselineskip}\selectfont}
\newcommand{\xiaosi}{\fontsize{12pt}{\baselineskip}\selectfont}
\newcommand{\wuhao}{\fontsize{10.5pt}{\baselineskip}\selectfont}
\newcommand{\xiaowu}{\fontsize{9pt}{\baselineskip}\selectfont}


```

修改标记英变中：

```
\renewcommand\figurename{\wuhao{图}}  %图的下标说明
\renewcommand\refname{\heiti{\sihao{参考文献}}} % 参考文献变成中文格式
\renewcommand{\baselinestretch}{1.5} %定义行间距
\renewcommand{\tablename}{\wuhao{表}
```

section格式修改

```

\usepackage{titlesec}
\usepackage{titletoc}
\renewcommand\thesection{第{\chinese{section}}章}
\renewcommand\thesubsection{\arabic{section}.\arabic{subsection}}
\renewcommand\thesubsubsection{\arabic{section}.\arabic{subsection}.\arabic{subsubsection}}
\renewcommand\theparagraph{({\arabic{paragraph}})}
\titleformat{\section}[block]{\centering\xiaosan\heiti\bfseries}{\thesection}{1em}{}
\titleformat{\subsection}[block]{\centering\sihao\heiti\bfseries}{\thesubsection}{1em}{}
\titleformat{\subsubsection}[block]{\xiaosi\heiti\bfseries}{\thesubsubsection}{1em}{}
\titleformat{\paragraph}{\xiaosi\heiti}{\theparagraph}{1em}{}

```

这里，更改section为第一章格式，更改后发现subsection随之变为第一章.1，因此重设subsection、subsubsection格式。然后发现latex没有subsubsubsection，即没有四级标题，取而代之的为paragraph，这里重设paragraph为（1）格式。
设置完section标号的格式之后，是设置section显示的字体，按照要求设置一下即可正确显示。






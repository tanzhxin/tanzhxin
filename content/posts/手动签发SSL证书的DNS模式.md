---
title: "手动签发SSL证书的DNS模式"
date: 2020-04-09T17:04:13+08:00
draft: false
---

为了签发SSL证书，可以通过DNS-1认证模式，手动通过域名服务认证


### 第一步： 安装acme.sh  
```  
curl https://get.acme.sh | sh
source ~/.bashrc
```


### 第二步： 初次申请要求认证，获得challenge key，记录好，用于下一步填写
```
acme.sh --issue -d www.example.com --dns  --yes-I-know-dns-manual-mode-enough-go-ahead-please
```

### 第三步： 登录域名服务器，添加txt类型_acme-challenge.www解析记录xxxxxxxxxxxxxxxxxx，并测试
```
dig -t TXT  _acme-challenge.www.example.com
```

### 第四步： 认证，并获取证书

```
acme.sh --renew -d www.example.com  --yes-I-know-dns-manual-mode-enough-go-ahead-please
```

### 第五步： 添加段，配置nginx.conf，使用SSL证书

```

server {
    listen              443 ssl;
    server_name         www.example.com;
    ssl_certificate     www.example.com.crt;
    ssl_certificate_key www.example.com.key;
    ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers         HIGH:!aNULL:!MD5;
    ...
}

```

完工!





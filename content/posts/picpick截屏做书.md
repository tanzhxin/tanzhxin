---
title: "Picpick截屏做书"
date: 2019-07-07T09:45:16+08:00
draft: false
---


做了两本，有些资料确实很难下载，doc88之类的却又收费，无奈只能自己动手丰衣足食。

picpick还真不错，能够自定义文件名并按序号自己排列好。当然最好是把显示屏旋转一下，可以获得更好的分辨效果。

剩下的就是处理图片，用imagemagick批量转换成pdf页面，然后pdftk打包成一个书籍文件。

```
for file in *.png; do convert $file ${file%%.*}.pdf; done
```





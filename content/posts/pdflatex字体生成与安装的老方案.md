---
title: "Pdflatex字体生成与安装的老方案"
date: 2019-11-06T20:18:44+08:00
draft: false
---



安装的过程：
Install TeXLive
_________________
First, install TeXLive.

wget ftp://ftp.tsinghua.edu.cn/mirror/CTAN/systems/texlive/Images/texlive2007-live-20070212.iso.zip
unzip texlive2007-live-20070212.iso.zip
mount -o loop texlive2007-live-20070212.iso /mnt
cd /mnt
sudo ./install-tl

press I to install.

Setup System Path
_________________
Set the path. Take debian GNU/Linux as example, modify your /etc/environment like this and make a reboot:

PATH="/usr/local/texlive/2007/bin/i386-linux:/usr/local/matlab/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/bin/X11:/usr/games"
LANG="en_US.UTF-8"
LANGUAGE="en_US.UTF-8"

Copy The Files Needed
_________________
make a dir called font, copy the font here.
mkdir ~/font
cd ~/font
cp /media/sda/windows/Fonts/simhei.ttf .

copy all the files needed.
sudo apt-get install fontforge
cp /usr/local/texlive/2007/texmf-dist/source/latex/CJK/utils/subfonts/* ~/font/
cp /usr/local/texlive/2007/texmf/fonts/sfd/*.sfd ~/font/





Generate Font
______________
Then, make the font. It is a good way to test how fast your computer is :) it takes me 3 minutes to generate the fonts[Core Duo 2].

Some one complain that the procedure take them 1 hours to do so, this is because you use the new version of fontforge. so be sure to use fontforge 2005.

time fontforge -script subfonts.pe simhei.ttf hei Unicode.sfd

create a file name makemap like this:

for i in *.tfm
do
cat >> hei.map << EOF
${i%.tfm} ${i%.tfm} < ${i%.tfm}.pfb
EOF
done

Make map file:
chmod +x makemap
./makemap

create a file name c70hei.fd for CJK package:

% This is c70hei.fd for CJK package.
% created by Edward G.J. Lee
% modify by Yue Wang
/ProvidesFile{c70hei.fd}
/DeclareFontFamily{C70}{hei}{/hyphenchar /font/m@ne}
/DeclareFontShape{C70}{hei}{m}{n}{<-> CJK * hei}{}
/DeclareFontShape{C70}{hei}{bx}{n}{<-> CJKb * hei}{/CJKbold}
/endinput

Copy Fonts into  TEXMF
_________________
create the local directory to save the font
cd ~/.texlive2007
cd texmf-var
mkdir -p fonts/map/dvips/CJK
mkdir -p fonts/tfm/CJK/hei
mkdir -p fonts/type1/CJK/hei
mkdir -p tex/latex/CJK/UTF8
cp ~/font/hei.map fonts/map/dvips/CJK/
cp ~/font/*.tfm fonts/tfm/CJK/hei
cp ~/font/*.pfb fonts/type1/CJK/hei
cp ~/font/c70hei.fd tex/latex/CJK/UTF8

Update The System and Test
_____________________
Just run:
texhash
updmap --enable Map hei.map
create a test file to test your work.

/documentclass{article}
/usepackage{CJKutf8}
/begin{document}
/begin{CJK}{UTF8}{hei}
你好!
/end{CJK}
/end{document}

latex test.tex
xdvi test.dvi
dvipdfm test.dvi
xpdf test.pdf
pdflatex test.pdf
......

Virtual GBK Font(Not Essential)
________________________

The main goal of virtual fonts is to make one font for various encoding systems. We still take simhei as an example to tell the reader how to create one UTF8 font for UTF8 and GBK encodings.

first, Let's suggest that you have finished the task of creating a utf8 heiti font for LaTeX and you are still in the ~/font directory with all the fonts and scripts undeleted. you also copy all the utf8 fonts into your ~/.texlive2007 directory and they are working with no problem.

run these commands. if you do not delete something , *.tfm and uni2sfd.pl are still in your ~/font dir.
perl uni2sfd.pl hei UGBK.sfd gbkhei gbk
mkdir ~/.texlive2007/texmf-var/fonts/tfm/CJK/gbkhei
mv gbkhei*.tfm ~/.texlive2007/texmf-var/fonts/tfm/CJK/gbkhei
mkdir ~/.texlive2007/texmf-var/fonts/vf
mv gbkhei*.vf ~/.texlive2007/texmf-var/fonts/vf
mkdir ~/.texlive2007/texmf-var/tex/latex/CJK/GBK

create a file named c19hei.fd just like the c70hei.fd in
~/.texlive2007/texmf-var/tex/latex/CJK/GBK

% This is c19hei.fd for CJK package.
% created by Edward G.J. Lee
/ProvidesFile{c19hei.fd}
/DeclareFontFamily{C19}{hei}{/hyphenchar /font/m@ne}
/DeclareFontShape{C19}{hei}{m}{n}{<-> CJK * gbkhei}{}
/DeclareFontShape{C19}{hei}{bx}{n}{<-> CJKb * gbkhei}{/CJKbold}
/endinput

make a test file in *GBK* encoding like this:

/documentclass{article}
/usepackage{CJK}
/begin{document}
/begin{CJK}{GBK}{hei}
你好
/end{CJK}
/end{document}

if you are lucky, you will get the good outputs without problem (no texhash or updmap needed)

Use ttf directedly in pdftex/dvipdfmx[not recommended]
----------------------------------------------------
Well, our pdflatex/dvipdfmx can also use ttf directedly.
create ~/.texlive2007/texmf-var/fonts/map/dvipdfm/cid-x.map

gbkhei@UGBK@    UniGB-UCS2-H    :0:simhei.ttf
hei@Unicode@    unicode    :0:simhei.ttf

move the font simhei.ttf to the truetype directory:
mkdir ~/.texlive2007/texmf-var/fonts/truetype
cp simhei.tff ~/.texlive2007/texmf-var/fonts/truetype


create the map generating file makemap-enc

for i in hei*.tfm
do
cat >> hei-enc.map << EOF
${i%.tfm} < ${i%.tfm}.enc < simhei.ttf
EOF
done

generate the map:
chmod +x makemap-enc
./makemap-enc

copy all the file needed
cp hei-enc.map ~/.texlive2007/texmf-var/fonts/map/pdftex/CJK/
mkdir -p ~/.texlive2007/texmf-var/fonts/enc/CJK/hei
cp hei*.enc ~/.texlive2007/texmf-var/fonts/enc/CJK/hei

copy the adobe font:
sudo mkdir ~/.texlive2007/texmf-var/fonts/cmap
sudo apt-get install cmap-adobe-*
cp -a /usr/share/fonts/cmap/* ~/.texlive2007/texmf-var/fonts/cmap

test that it works:

The UTF8 encoding test file:

/pdfoutput=1
/pdfmapfile{=hei-enc.map}
/documentclass{article}
/usepackage{CJKutf8}
/begin{document}
/begin{CJK}{UTF8}{hei}
你好
/end{CJK}
/end{document}

The GBK encoding test file(save it in gbk first!):

/pdfoutput=1
/pdfmapfile{=hei-enc.map}
/documentclass{article}
/usepackage{CJK}
/begin{document}
/begin{CJK}{GBK}{hei}
你好
/end{CJK}
/end{document}

Install Ctex macro package
-----------------------------------------

There are two versions of ctex macro package, one for  GBK and one for UTF-8.
First, be sure to install all the chinese fonts using simsun.ttf(If you use
simsun.ttc, break it) simhei.ttf, simli.ttf, simkai.ttf, simfang.ttf,
simyou.ttf just follow the example of simhei before.

then, copy all the macropackages in Liangzi's CTeXLive-2005
sudo mount -o loop  CTeXLive2005  /mnt
cp -a /mnt/2005/texmf-local/tex/latex/ctex ~/.texlive2007/texmf-var/tex/latex
cp -a /mnt/2005/texmf-local/tex/latex/ctexutf8
~/.texlive2007/texmf-var/tex/latex
cp -a /mnt/2005/texmf-local/tex/latex/ccmap ~/.texlive2007/texmf-var/tex/latex

modify all the fd files there.
for those fd file in ctexutf8 directory, use song,fang ... instead of unisong,
unifang, unisongsl,unifangsl....., for those fd files in ctex directory, use
gbksong instead of gbksongsl, gbkfang instead of gbkfangsl....
And dont use /CJKbold , use hei instead.

an example file should like:
in utf8 directory

/ProvidesFile{c70rm.fd}
  [2006/06/09 v0.8 ctex
     font definition file]

/DeclareFontFamily{C70}{rm}{/hyphenchar /font/m@ne}
/DeclareFontShape{C70}{rm}{m}{n}{<-> CJK * song}{}
/DeclareFontShape{C70}{rm}{bx}{n}{<-> CJK * hei}{}
/DeclareFontShape{C70}{rm}{m}{sl}{<-> CJK * song}{}

in ctex directory

/ProvidesFile{c19sf.fd}
  [2006/06/09 v0.8 ctex
     font definition file]
     /DeclareFontFamily{C19}{sf}{/hyphenchar /font/m@ne}
     /DeclareFontShape{C19}{sf}{m}{n}{<-> CJK * gbkyou}{}
     /DeclareFontShape{C19}{sf}{bx}{n}{<-> CJKb * gbkhei}{}
     /DeclareFontShape{C19}{sf}{m}{sl}{<-> CJK * gbkyou}{}
     /DeclareFontShape{C19}{sf}{bx}{sl}{<-> CJKb * gbkhei}{}
     /DeclareFontShape{C19}{sf}{m}{it}{<-> CJK * gbkyou}{}
     /DeclareFontShape{C19}{sf}{bx}{it}{<-> CJKb * gbkhei}{}
     /endinput

Last,run texhash and test the files.
Utf8 test file

/documentclass{ctexreputf8}
/begin{document}
你好
/end{document}

 GBK test file

/documentclass{ctexrep}
/begin{document}
你好
/end{document}


上面给出了安装texlive的方法以及如何为texlive安装中文字体。此处谨以hei体为例进行了说明

可以看出上面的字体制作过程相当的繁琐。利用脚本可以自动化这个过程

下面是两个脚本的内容：ctexlive 用法 ctexlive simsun.ttf song
===============================ctexlive begin=========================
#!/bin/bash

# $1 source ttf file, such as /media/wind/WINDOWS/Fonts/simkai.ttf
# $2 the name of font, such as song, kai, hei, fang, and etc.

# use format, ./ctexlive /media/wind/WINDOWS/Fonts simsun.ttc song

# for UTF8. /usepackage{CJKutf8} and /begin{CJK}{UTF8}{}

# prepare directory and files
mkdir -p ~/font
cd ~/font
cp /usr/local/texlive/2007/texmf-dist/source/latex/CJK/utils/subfonts/* ~/font/
cp /usr/local/texlive/2007/texmf/fonts/sfd/*.sfd ~/font/
mkdir -p $2
cd $2

# generate font
time fontforge -script ../subfonts.pe $1 $2 ../Unicode.sfd

# create map file
for i in *.tfm
do
cat >> $2.map << EOF
${i%.tfm} ${i%.tfm} < ${i%.tfm}.pfb
EOF
done

# create fd file
cat >> c70$2.fd << EOF
% This is c70$2.fd for CJK package.
% created by Edward G.J. Lee
% modify by Yue Wang
/ProvidesFile{c70$2.fd}
/DeclareFontFamily{C70}{$2}{/hyphenchar /font/m@ne}
/DeclareFontShape{C70}{$2}{m}{n}{<-> CJK * $2}{}
/DeclareFontShape{C70}{$2}{bx}{n}{<-> CJKb * $2}{/CJKbold}
/endinput
EOF

# Copy Fonts into TEXMF
cd ~/.texlive2007/texmf-var
mkdir -p fonts/map/dvips/CJK
mkdir -p tex/latex/CJK/UTF8
mkdir -p fonts/tfm/CJK/$2
mkdir -p fonts/type1/CJK/$2
cp ~/font/$2/$2.map fonts/map/dvips/CJK/
cp ~/font/$2/c70$2.fd tex/latex/CJK/UTF8
cp ~/font/$2/*.tfm fonts/tfm/CJK/$2
cp ~/font/$2/*.pfb fonts/type1/CJK/$2

# Update The System
texhash
updmap --enable Map $2.map

# for GBK. /usepackage{CJK} and /begin{CJK}{GBK}{song}
cd ~/font/$2
perl ../uni2sfd.pl $2 ../UGBK.sfd gbk$2 gbk
mkdir -p ~/.texlive2007/texmf-var/fonts/tfm/CJK/gbk$2
mv gbk$2*.tfm ~/.texlive2007/texmf-var/fonts/tfm/CJK/gbk$2
mkdir -p ~/.texlive2007/texmf-var/fonts/vf
mv gbk$2*.vf ~/.texlive2007/texmf-var/fonts/vf
mkdir -p ~/.texlive2007/texmf-var/tex/latex/CJK/GBK

# create fd file for gbk

cd ~/.texlive2007/texmf-var/tex/latex/CJK/GBK

cat >> c19$2.fd << EOF
% This is c19$2.fd for CJK package.
% created by Edward G.J. Lee
/ProvidesFile{c19$2.fd}
/DeclareFontFamily{C19}{$2}{/hyphenchar /font/m@ne}
/DeclareFontShape{C19}{$2}{m}{n}{<-> CJK * gbk$2}{}
/DeclareFontShape{C19}{$2}{bx}{n}{<-> CJKb * gbk$2}{/CJKbold}
/endinput
===============================ctexlive end=========================

用法:mkfont.sh simsun.ttf simsun song
==============================mkfont.sh begin======================
FULLNAME=$1
TTF2PT1=`which ttf2pt1`
DATE=`date`
TEXMF=~/texmf

if [ $# -eq 3 ]
then
  FNAME=`basename $FULLNAME`
    eval `echo $FNAME | awk -F. '{printf "FHEAD=%s;FTAIL=%s",/$1,/$2}'`
      FHEAD=$3
        FONTNAME=$3
	else
	  echo "Usage: `basename $0` your.ttf subfont_name font_name"
	    exit
	    fi

check_enc()
{
    NUMLIST=`awk 'BEGIN{ n=1; while(n<256){printf "%02x/n",n; n++}}'`
        MAP=unicode-sample.map
	    UTF8ENC=70
	        GBKENC=19
		    UTF8FD=${TEXMF}/tex/latex/CJK/UTF8
		        GBKFD=${TEXMF}/tex/latex/CJK/GBK
			}

create_type1()
{
  echo "Now create *.pfb and *.enc files, wait... "
    for i in $NUMLIST
      do
          ttf2pt1 -GAE -pttf -OHUBs -W0 -l plane+pid=3,eid=1,0x$i $FULLNAME ${FHEAD}$i
	    done

# avoid dvips t1part module bugs.
  perl -pi -e 's/_/Z/g' *.t1a *.afm

  for ps in *.t1a
    do
        t1asm -b $ps > ${ps%.t1a}.pfb
	  done
	  }

create_map()
{
MAPFILE=t1-${FHEAD}.map
ENCMAP=ttf-${FHEAD}.map
  echo "Create *.tfm and (dvips)map file, wait..."
    cat > $MAPFILE << EndOfFile
    % This is map file for dvips/dvipdfm[x] and LaTeX CJK package.
    % Created by Edward G.J. Lee <edt1023@info.sayya.org>
    % $DATE
    EndOfFile
      cat > $ENCMAP << EndOfFile
      % This is map file for PDFLaTeX and LaTeX CJK package to embed TTF.
      % Created by Edward G.J. Lee <edt1023@info.sayya.org>
      % $DATE
      EndOfFile
        for i in $NUMLIST
	  do
	      PSNAME=`awk '/FontName/ {print $2}' ${FHEAD}$i.afm`
	          afm2tfm ${FHEAD}$i.afm > /dev/null 2>&1
		      cat >> $MAPFILE << EndOfFile
		      ${FHEAD}$i $PSNAME <${FHEAD}$i.pfb
		      EndOfFile
		          cat >> $ENCMAP << EndOfFile
			  ${FHEAD}$i <${FHEAD}$i.enc <${FNAME}
			  EndOfFile
			    done
			    }

create_cidmap()
{
cat >> cid-x.map << EndOfFile
gbk$FHEAD@UGBK@ UniGB-UCS2-H :0:$FNAME
$FHEAD@Unicode@ unicode :0:$FNAME
EndOfFile
}

create_cjkfd()
{
cat > c${UTF8ENC}${FONTNAME}.fd << EndOfFile
/ProvidesFile{c${UTF8ENC}${FONTNAME}.fd}[/filedate/space/fileversion]
/DeclareFontFamily{C${UTF8ENC}}{$FONTNAME}{/hyphenchar /font/m@ne}
/DeclareFontShape{C${UTF8ENC}}{$FONTNAME}{m}{n}{<-> CJK * $FHEAD}{}
/DeclareFontShape{C${UTF8ENC}}{$FONTNAME}{bx}{n}{<-> CJKb * $FHEAD}{/CJKbold}
/endinput
EndOfFile

cat > c${GBKENC}${FONTNAME}.fd << EndOfFile
/ProvidesFile{c${GBKENC}${FONTNAME}.fd}[/filedate/space/fileversion]
/DeclareFontFamily{C${GBKENC}}{$FONTNAME}{/hyphenchar /font/m@ne}
/DeclareFontShape{C${GBKENC}}{$FONTNAME}{m}{n}{<-> CJK * gbk$FHEAD}{}
/DeclareFontShape{C${GBKENC}}{$FONTNAME}{bx}{n}{<-> CJKb * gbk$FHEAD}{/CJKbold}
/endinput
EndOfFile
}

create_vf()
{
  echo "Create virtual fonts file, wait..."
  perl uni2sfd.pl $FHEAD UGBK.sfd gbk$FHEAD gbk
  }

# main()
check_enc
create_type1
create_map
create_cidmap
create_cjkfd
create_vf

AFM=${TEXMF}/fonts/afm/$FHEAD
TFM=${TEXMF}/fonts/tfm/$FHEAD
PFB=${TEXMF}/fonts/type1/$FHEAD
ENC=${TEXMF}/fonts/enc/$FHEAD
VF=${TEXMF}/fonts/vf/$FHEAD
TTF=${TEXMF}/fonts/truetype/$FHEAD
MAPDIR=${TEXMF}/fonts/map
rm -f *.t1a
mkdir -p $AFM $TFM $PFB $ENC $VF $TTF
mv -f *.enc $ENC
mv -f *.afm $AFM
mv -f *.tfm $TFM
mv -f *.pfb $PFB
mv -f *.vf $VF
mv -f $FNAME $TTF
mkdir -p $MAPDIR/dvips
mkdir -p $MAPDIR/pdftex
mkdir -p $MAPDIR/dvipdfm
mv -f $MAPFILE $MAPDIR/dvips
mv -f $ENCMAP $MAPDIR/pdftex
cat  cid-x.map >> $MAPDIR/dvipdfm/cid-x.map
rm cid-x.map
mkdir -p $UTF8FD $GBKFD
mv -f c${UTF8ENC}${FONTNAME}.fd $UTF8FD
mv -f c${GBKENC}${FONTNAME}.fd $GBKFD

echo "Running texhash and updmap, pls wait"
texhash >/dev/null 2>&1
updmap --enable Map=t1-${FHEAD}.map >/dev/null 2>&1

echo "Congradulations! you have added ${FHEAD} into your texmf"
==============================mkfont.sh end========================

更省事的办法是直接下载YueWang-zhfonts-final_1.01.tar.bz2

這個是針對TeXLive2007的字體包,總的來說,它解決了以下的問題:

安好了UTF8和GBK的中易和方正字體,
字體名為fzfsk, fzhtk, fzktk, fzlsk, fzssk, fzy1k,
simfang, simhei, simkai, simli, simsun, simyou.
並使用sim字體作為默認的song,you,kai,li,hei,fs字體.
可以/begin{CJK}{UTF8}{simsun}或/begin{CJK}{UTF8}{song}來使用.

帶有CTeX和CTeXutf8宏包,可以這樣加載:
/documentclass{ctexart}或/documentclass{ctexartutf8}
當然,也可以用老的方法加載,比如
/documentclass{article}
/usepackage{ctexutf8,ctexreputf8}
默認使用sim系列的字體,若要用方正字體,可以改變ctex的fd檔.

使用了最新的beamer,並且給beamer打了utf8的patch.現在和pdftex不兼容,無法使用utf8的問題應該沒有了.

加了adobe的四個中文opentype 字體,如要使用請修改cid-x.map

修正了texlive 2007中mathtime和lucidar字體的bug,見tug.org/texlive/的bug list.

修正了liangzi在ctexutf8的一個bug

修正了ctex和ctexutf8的fd檔定義不全的問題.

帶上了ccmap,可以在使用gbk編碼時生成正確的pdf

帶上了thuthesis 3.0版本,不過好象Leo沒有做utf8版本?

安裝方法應該
cd ~
mv .texlive2007 .texlive2007.bak
tar jxvf YueWang-zhfonts-final.tar.bz2
就可以了.
如果不行,請把~/.texlive2007/texmf-var/fonts/map/dvips中的所有map給enable了.


/begin{verbatim}
/documentclass{article}
/usepackage{<cjkpackage>}
/begin{document}
/begin{CJK}{<encoding>}{<fontname>}
你好!
/end{CJK}
/end{document}
/end{verbatim}

上面是模板
其中<cjkpackage>可以是CJKutf8(源文件用UTF8编码)也可以是CJK(源文件用GBK编码)
<encoding>是GBK或者UTF8和上面的 <cjkpackage>是对应的
也就是cjkpackage是CJK则encoding必须是GBK，而如果cjkpackage是CJKutf8,则encoding就是UTF8

fontname就好说了，就是song hei fs kai等等




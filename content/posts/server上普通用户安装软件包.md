---
title: "Server上普通用户安装软件包"
date: 2020-05-15T14:32:48+08:00
draft: false
---


# find the package in rpmfind.net and download it.

[rpmfind](Http://rpmfind.net/)
[pkgs](https://pkgs.org/)


# Install in user space:

```
rpm2cpio zip-3.0-11.el7.x86_64.rpm | cpio -idvm

```

# include path in .bashrc:

```
export ZIP_HOME=/home/hadoop/zip
export PATH=$ZIP_HOME/bin:$PATH
source .bashrc

```


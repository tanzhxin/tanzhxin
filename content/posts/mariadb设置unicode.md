---
title: "Mariadb设置unicode"
date: 2020-06-05T17:05:37+08:00
draft: false
---



![Changing the Default Character Set To UTF-8](https://mariadb.com/kb/en/setting-character-sets-and-collations/)

编辑my.cnf

```

[client]
...
default-character-set=utf8mb4
...
[mysql]
...
default-character-set=utf8mb4
...
[mysqld]
...
collation-server = utf8mb4_unicode_ci
init-connect='SET NAMES utf8mb4'
character-set-server = utf8mb4
...


```



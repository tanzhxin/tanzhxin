---
title: "Software软件列表"
date: 2019-02-19T18:17:42+08:00
draft: false
---


## 图形图像

imageJ

paint.net

irfanview

picasa


## 文字处理


office

pspad

notepad++ 

emacs

texlive + texstudio

winmerge 

jmeld

overleaf

github

## 文献管理

acroread

winDjview

calibre


kindle

sumatra PDF

jabref 

endnoteX (x)

github

mendeley

webofScience

## 搜索

docfetcher

searx

mezw

locate


## 词典

artha

golden-dict

bing translator

## 视频影音

vlc

kmplayer

MPC-BEx64

CNTV

foobar

bilibili


## 远程登陆

putty

msys2

xming 

vcxsrv



## 编程调试

cmake3

git

qt-online 

vs 2017

vcpkg

vscode

anaconda 

ssh + emacs 

## 工具

filezilla

irssi

lftp

wget curl 

dupeguru

freemind



## 专业

root 5.34.36

geant4

andor solis

comsol

solidwork

gnuplot

windriver

yED

vidyo

RIGOL

tesmviewer

matlab

OpticalRayTracer

Thorlabs


## 其他

7-zip

chrome

firefox + bitwarden

TIM

wechat

aliwangwang

钉钉


scratch

ihepbox

winfsp + sirikari

pdftkX

rufus

CrystalDiskInfo  

diskgenius

CSR bt4.0

Heidi SQL

inkscape

ghostscript 

nvidia

Qiku phone helper


sslvpn: easyconnection

pan.baidu.com

cloud.189.com

epson label
  [label editor](http://www.epson.com.cn/Apps/tech_support/GuideDrive.aspx?columnid=384&ptype=1031&pmodel=31073)






+++ 
draft = false
date = 2021-08-23T19:36:24+08:00
title = "dotemacs配置"
description = ""
slug = ""
authors = []
tags = []
categories = []
externalLink = ""
series = []
+++



1. 设置系统

```
;; Emacs Server

(server-start)

;; PDF-Tools
(pdf-tools-install)


(setq user-full-name    "xxx") 
(setq user-mail-address "xxx@xxx.cn")
;(setenv "HOME" "C:/Users/xxx/")
;(setenv "PATH" "C:/Users/xxx/")

;;set the default file path
(setq default-directory "~/xxxxcloud/")
;(add-to-list 'load-path "~/emacs/site-lisp")

(prefer-coding-system 'utf-8) 
(set-language-environment "UTF-8")
;(setq browse-url-generic-program (executable-find "firefox"))
;(setq browse-url-browser-function 'browse-url-generic)
;(setq browse-url-browser-function 'browse-url-chromium)


```

2. 字体
```
(set-face-attribute 'default nil :font "consolas-14:weight=normal")
  (dolist (charset '(kana han cjk-misc bopomofo))
    (set-fontset-font (frame-parameter nil 'font) charset
		      (font-spec :family "微软雅黑" :size 16)))

;(setq-default line-spacing 1.05)   

```

3. 包管理
```
(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives '(("gnu" . "http://mirrors.ustc.edu.cn/elpa/gnu/")
                         ("melpa" . "http://mirrors.ustc.edu.cn/elpa/melpa/")
;                         ("melpa-stable" . "http://mirrors.ustc.edu.cn/elpa/melpa-stable/")
                         ("org" . "http://mirrors.ustc.edu.cn/elpa/org/")))
(package-initialize)

```


4. 界面

```
(load-theme 'zenburn t)

;(global-display-line-numbers-mode)
;(window-numbering-mode t)

(setq visible-bell t)    
(show-paren-mode t)
(blink-cursor-mode 1)

(setq inhibit-startup-message t)
(setq initial-scratch-message "")
(fset 'yes-or-no-p 'y-or-n-p)


(setq inhibit-startup-screen t)
(setq inhibit-startup-echo-area-message t)
(setq inhibit-startup-message t)
(setq initial-scratch-message nil)
(setq initial-major-mode 'org-mode)
(setq-default indent-tabs-mode nil)
(setq pop-up-windows nil)


(tool-bar-mode 0)
(tooltip-mode 0)
(scroll-bar-mode 0)
(menu-bar-mode 1)


```

5. 最近访问文件
```
(require 'recentf)
(recentf-mode 1)
;; (setq recentf-save-file "~/.emacs.d/.recentf")
(setq recentf-max-saved-items 60)
(setq recentf-max-menu-items 60)

```


6. 文件夹
```
(setq dired-recursive-deletes 'always)
(setq dired-recursive-copies 'always)
(put 'dired-find-alternate-file 'disabled nil)

```

7.  语法检查

;  (add-hook 'after-init-hook 'global-flycheck-mode)
(setenv "LANG" "en_US")
(setq ispell-program-name
      "~/bin/hunspell.exe")

8. ido
```
(require 'ido)
(setq ido-enable-flex-matching t)
(setq ido-use-filename-at-point 'guess)
(setq ido-everywhere t)
(ido-mode 1)

```

9. 自动保存
```
;; Avoid #file.org# to appear
(auto-save-visited-mode)
(setq create-lockfiles nil)
;; Avoid filename.ext~ to appear
(setq make-backup-files nil)

```

10. 文学编程

```
 (org-babel-do-load-languages
       'org-babel-load-languages
       '((emacs-lisp . t)
         (C . t)
   	 (js . t)
         (python . t)
         (shell . t)
         (latex . t)
         (R . t)))

```

11. org-roam笔记

```

(setq org-roam-v2-ack t)
(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory (file-truename "~/Nextcloud/orgroam/"))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ("C-c n j" . org-roam-dailies-capture-today))  ;; Dailies
  :config
  (org-roam-db-autosync-mode)
  (require 'org-roam-protocol)  )                      ;; If using org-roam-protocol


;; manual install org-roam-ui, after init run org-roam-ui-mode to view the ball
(add-to-list 'load-path "~/.emacs.d/org-roam-ui")
(load-library "org-roam-ui")       


```


12. mu4e邮件

;(setq mu4e-html2text-command "w3m -dump -T text/html")





13. ivy and swiper

```
;; Ivy,Counsel, & Swiper
;;; Enable Ivy mode in general
(ivy-mode 1)
(setq ivy-use-virtual-buffers t)
(setq enable-recursive-minibuffers t)

;;; Add Counsel and Swiper search functions
(global-set-key (kbd "C-c f r") #'counsel-recentf)
(global-set-key (kbd "C-c C-s") #'swiper)

;;; Reple default "M-x" and "C-x C-f" with Counsel version
(global-set-key (kbd "M-x") #'counsel-M-x)
(global-set-key (kbd "C-x C-f") #'counsel-find-file)

;; Optionally, you can replace these default functions with Counsel enhanced ones
;;(global-set-key (kbd "C-h f") 'counsel-describe-function)
;;(global-set-key (kbd "C-h v") 'counsel-describe-variable)



```


14. pdf-tools
```
(use-package pdf-tools
   :pin manual
   :config
   (pdf-tools-install)
   (setq-default pdf-view-display-size 'fit-width)
   (define-key pdf-view-mode-map (kbd "C-s") 'isearch-forward)
   :custom
   (pdf-annot-activate-created-annotations t "automatically annotate highlights"))

(setq TeX-view-program-selection '((output-pdf "PDF Tools"))
      TeX-view-program-list '(("PDF Tools" TeX-pdf-tools-sync-view))
      TeX-source-correlate-start-server t)

(add-hook 'TeX-after-compilation-finished-functions
          #'TeX-revert-document-buffer)

(add-hook 'pdf-view-mode-hook (lambda() (linum-mode -1)))



```

15. elfeed信息推送
```
;(global-set-key (kbd "C-x w") 'elfeed)
(setq elfeed-feeds
      '( ;("https://www.mysmth.net/nForum/rss/board-QingJiao" qingjiao)
        ("http://www.solidot.org/index.rss"  solidot)
	;("https://feed.iplaysoft.com/" iplaysoft)
	;("https://sspai.com/feed" sspai)
	("https://linux.cn/rss.xml" linuxcn)
        ("http://nedroid.com/feed/" webcomic) 
        ("http://nullprogram.com/feed/" nullprogram)         
        ("https://planet.emacslife.com/atom.xml" emacslife) ) )


(setf url-queue-timeout 60)

(provide 'init-elfeed)  

```






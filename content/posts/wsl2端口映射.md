+++ 
draft = false
date = 2021-08-09T21:07:09+08:00
title = "wsl2端口转发"
description = ""
slug = ""
authors = []
tags = []
categories = []
externalLink = ""
series = []
+++




获取虚拟机内ubuntu的ip地址

wsl -- ifconfig eth0


首先找到WSL的虚拟网卡地址，具体每台设备可能不同，此处假设是：172.24.39.57

打开 PowerShell，添加端口转发：

netsh interface portproxy add v4tov4 listenport=80 connectaddress=172.24.39.57 connectport=80 listenaddress=* protocol=tcp

检测是否设置成功

netsh interface portproxy show all


如果删除端口转发，执行：

netsh interface portproxy delete v4tov4 listenport=80 protocol=tcp





windows 启动后可能为WSL分配的IP变了，linux子系统的IP也变了，可以给windows中WSL网络适配器加一个固定的IP:


or  分别执行这两个命令：

netsh interface ip add address "vEthernet (WSL)" 172.24.39.1 255.255.0.0

ip addr add 172.24.39.57/16 broadcast 172.24.39.255 dev eth0 label eth0:1







# 给wsl 中的ubuntu 设置ipv4 的ip 192.168.86.16，要在windows中访问wsl就用此ip

wsl -d Ubuntu -u root ip addr add 192.168.86.16/24 broadcast 192.168.50.255 dev eth0 label eth0:1

# 给windows 设置ipv4 的ip 192.168.86.88，要在wsl中访问宿主机就用此

ipnetsh interface ip add address "vEthernet (WSL)" 192.168.86.88 255.255.255.0




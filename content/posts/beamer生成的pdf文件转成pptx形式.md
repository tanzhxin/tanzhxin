---
title: beamer生成的pdf文件转成pptx形式
date: 2019-11-26T10:56:12+08:00
draft: false
---

历经千辛万苦用beamer做完presentation后，被秘书告知考核报告需要计时，因此必须是pptx格式，因此考虑用pdf转图片然后再打包成pptx文件

思路是这样，用pdftk把pdf文件解开成单张，然后是imagemagick将单张pdf转成高分辨率的png，最后用python-pptx把一堆png文件添加成pptx文件。

```
pdftk  vice.pdf  burst
```

好像现在还有其他工具，比如pdfsam

上传到linux服务器后，用bash开始批处理
```
for files in ls *.pdf; do convert -density 900 $files  ${files%.pdf}.png;  done
```


最后是python脚本

```
import os;
import pptx;
from pptx.util import Inches;

picFiles= [fn for fn in os.listdir() if fn.endswith('.png')]
pptxFile = pptx.Presentation()

#for fn in sorted(picFiles, key=lambda item:int(item[:item.rindex('.')])):

slide = pptxFile.slides.add_slide(pptxFile.slide_layouts[1]); slide.shapes.add_picture('pg_0001.png', Inches(0), Inches(0), Inches(10), Inches(7.5));
pptxFile.save('vice.pptx')


#title_slide_layout = prs.slide_layouts[0]
#slide = prs.slides.add_slide(title_slide_layout) 
#title = slide.shapes.title
#subtitle = slide.placeholders[1] 
#title.text = "Hello, World!"
#subtitle.text = "pip install python-pptx"
#prs.save("test.pptx")

```

python3运行脚本后就可以看到pptx文件


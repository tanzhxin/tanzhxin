---
title: "Texlive2020安装"
date: 2020-05-17T18:02:54+08:00
draft: false
---


# 下载安装包
[texlive netinstall](https://tug.org/texlive/acquire-netinstall.html)

# 创建安装方案
```
./install-tl > Enter command: P
```

# 修改安装目录，重定义到用户主目录下
```
sed -i 's|/usr/local/|~/|g' texlive.profile 
```

# 重新开始网络安装
```
./install-tl -profile texlive.profile
```


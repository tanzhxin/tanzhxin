+++ 
draft = false
date = 2021-08-23T11:00:46+08:00
title = "postgresql简要"
description = ""
slug = ""
authors = []
tags = []
categories = []
externalLink = ""
series = []
+++

0. 安装数据库

   apt-get install -y  postgresql  postgresql-contrib


   测试：

   sudo -u postgres  psql -c  "select version();"


   打开服务端口 /etc/postgresql/12/main/posgresql.conf：

   listen_addresses = "*";


   授权用户访问：

   host    all             jb             0.0.0.0/0              md5


   授权端口访问：

   ufw allow proto tcp  from 0.0.0.0/0  to any port 5432;


   启动服务：

   service  postgresql start


   查验端口开放：


   ss -nlt


   


1. 创建数据库

   CREATE DATABASE dbname encoding='utf-8';

2. 创建表格

   CREATE TABLE table_name(
   	  column1 datatype,
	  column2 datatype,
          column3 datatype,
	   .....
	  columnN datatype,
	  PRIMARY KEY( 一个或多个列 )
      );

3. 创建用户

    create user username with password '****';

4. 授权数据库给用户

   grant all on database dbtest to username;

5. 修改密码：

   alter user  -USERNAME- with password  '****'


6.  插入数据条目

    INSERT INTO autorepairs VALUES ('2017-08-11', 'airbag recall', 'dealer', 0);

7.  更新数据条目

    SELECT date, repairs FROM autorepairs ORDER BY date;


8.  删除条目

    DELETE FROM autorepairs WHERE repairs = 'airbag recall';


9.  查询数据条目

    SELECT date, repairs FROM autorepairs ORDER BY date;


10.  设置输出到文件，然后重新运行select


      \o  /tem/11.csv

      或者

     COPY (select * from table) to '/tmp/tmp.csv' with csv header;


11.  从csv文件导入数据， copy

     COPY 表名 FROM 'CSV文件' WITH DELIMITER ',' NULL AS '' CSV HEADER QUOTE AS '"';



---
title: "Putty配色与密匙"
date: 2019-02-13T20:19:22+08:00
draft: false
---


1 配色问题，可以通过注册表文件指定任务的几种颜色来设置，具体如下：

{% codeblock   %}
Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\SimonTatham\PuTTY\Sessions\SESSION_NAME]

; Default Foreground
"Colour0"="217,216,216"
; Default Bold Foreground
"Colour1"="217,216,216"
; Default Background
"Colour2"="28,28,28"
; Default Background
"Colour3"="28,63,149"
; Cursor Text
"Colour4"="28,28,28"
; Cursor Color
"Colour5"="231,231,232"
; ANSI Black
"Colour6"="115,113,113"
; ANSI Black Bold
"Colour7"="115,113,113"
; ANSI Red
"Colour8"="251,38,8"
; ANSI Red Bold
"Colour9"="251,38,8"
; ANSI Green
"Colour10"="167,226,46"
; ANSI Green Bold
"Colour11"="167,226,46"
; ANSI Yellow
"Colour12"="102,217,238"
; ANSI Yellow Bold
"Colour13"="102,217,238"
; ANSI Blue
"Colour14"="0,157,220"
; ANSI Blue Bold
"Colour15"="0,157,220"
; ANSI Magenta
"Colour16"="255,85,255"
; ANSI Magenta Bold
"Colour17"="255,85,255"
; ANSI Cyan
"Colour18"="255,210,4"
; ANSI Cyan Bold
"Colour19"="255,210,4"
; ANSI White
"Colour20"="217,216,216"
; ANSI White Bold
"Colour21"="255,255,255"
{% endcodeblock%}

保存为reg文件后合并，注意将上述session name替换成自己的。

2 putty的密匙
 
   a. puttygen.exe导入自己的私匙，
   b. 保存私匙（ppk格式）
   c. session中auth中指定ppk私匙。
  
{% asset_img putty-ppk.PNG puttygen %}



3 连接保持
  见图，keepalive 20s
{% asset_img putty-connection.PNG putty-conn %}


4 字体及大小
  选择monaco字体，字体大小选择12。

5 全屏退出全屏的快捷键设置

   Window->Behaviour最下面有个Full screen on Alt-Enter，勾上就可以。




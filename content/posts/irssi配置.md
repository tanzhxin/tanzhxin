+++ 
draft = false
date = 2021-08-09T23:10:40+08:00
title = "irssi配置"
description = ""
slug = ""
authors = []
tags = []
categories = []
externalLink = ""
series = []
+++





[libera chat的参考指南](https://libera.chat/guides/irssi)

[archLinux的设置](https://wiki.archlinux.org/title/Irssi)

[irssi network命令](https://irssi.org/documentation/help/network/)

[startup顺序](https://irssi.org/documentation/startup/)

自动验证:

 /NETWORK ADD -autosendcmd "/^msg NickServ IDENTIFY password;wait 2000"  liberachat

[查找频道：](https://libera.chat/guides/findingchannels)

 /msg alis LIST searchterm

常用频道：

linuxba  docker  node.js  mongodb  redis  emacs  windows-wsl

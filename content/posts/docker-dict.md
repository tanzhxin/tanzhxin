---
title: 使用docker开启dictd中英文词典查询服务
date: 2024-02-18T10:31:12+08:00
draft: false
---

字典查询服务有点意思，

最初是想自己编译，在rockylinux服务器上费了牛劲从bmake开始安装，也算是成功了，但是实在是太累了。

还是从ubuntu镜像开始比较容易，中英文查询的langdao-ec词典可以从网上找找，其他都比较简单


```
FROM ubuntu
SHELL ["/bin/bash", "-c"]
COPY ./langdao-ec.* /usr/share/dictd/
RUN sed -i 's@//.*archive.ubuntu.com@//mirrors.ustc.edu.cn@g' /etc/apt/sources.list  \
  && apt-get update  \
  && apt-get install -y dict dictd dict-wn  nano iproute2 net-tools \
  && sed -i 's/127.0.0.1/*/g' /etc/dictd/dictd.conf \
  && dictdconfig -w
EXPOSE 2628/tcp
CMD ["dictd", "-dnodetach"]

```

注意：服务启动很慢，要等一会儿才能用客户端去连接测试


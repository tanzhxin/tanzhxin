---
title: "Pyside2的初始化问题"
date: 2021-04-14T14:39:17+08:00
draft: false
---

启动pyside2的项目时遇到错误找不到插件，下面是搜索到的解决方案，主要考虑直接修改pyside2系统

错误 qt.qpa.plugin: Could not find the Qt platform plugin “windows“ in ““ 的解决方法

在pyside2的__init__.py文件中直接添加：
```

import PySide2

dirname = os.path.dirname(PySide2.__file__) 
plugin_path = os.path.join(dirname, 'plugins', 'platforms')
os.environ['QT_QPA_PLATFORM_PLUGIN_PATH'] = plugin_path

```

测试项目启动正常，完工.





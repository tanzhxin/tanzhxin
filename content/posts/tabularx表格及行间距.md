---
title: "Tabularx表格及行间距"
date: 2020-04-07T17:59:57+08:00
draft: false
---

行间距设置用到的package是：\usepackage{setspace}

tabularx指定表格宽度，以及第一列宽度并自动平均剩余列宽度。


命令：
```
\begin{table}[tbh]
   %\centering
   \begin{spacing}{1.5}						
      \begin{tabularx}{15cm}{|p{1.8cm}| X| X |}  
         \hline
	 时间/ps   &  事件   &  结果   \\  \hline
	 0 $ \sim 10^{-6}$	   & 入射粒子传递反冲能量  &  初级离位原子  \\	  \hline		
	 $10^{-6} \sim $ 0.2   &  PKA慢化过程行程级联碰撞  &  空位、低能反冲原子、子级联  \\   \hline
	 0.2 $\sim$  0.3  &  离位峰的形成   &   低密度、热融化液滴、冲击波峰  \\   \hline
	 0.3 $\sim$ 3 &  离位峰弛豫，间隙原子的逐出，从热液核转变为过冷液滴  &  稳定的间隙原子，原子混合    \\   \hline
	 3 $\sim$ 10  & 离位峰固化，并冷却到环境温度  &  贫原子去，无序区，非晶区，空位团坍塌 \\  \hline
	 10$~$无穷大 & 从级联碰撞区中稳定的间隙原子核空位在级联区内部热运动复合核逸出，移出缺陷间相互作用  &  存活的缺陷、逸出的间隙原子、逸出䣌空位、间隙原子和空位到
	\end{tabularx}	
   \end{spacing}
\end{table}

```





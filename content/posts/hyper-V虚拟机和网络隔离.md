---
title: "hyper-V虚拟机和网络隔离"
date: 2021-03-27T07:04:21+08:00
draft: false
---


好久以前就发现virtualbox越来越慢，随着win10的更新，现在几乎不动弹，诶，不知道是不是MS又捣了什么鬼。想想有些特定需求还得用虚拟机阿，于是还只能考虑MS的hyper-V虚拟机。虽然hyper-v是win10内置，但是安装他需要软件和硬件两方面的支持

# 硬件-BIOS
hyper-v的安装有个条件是必须硬件支持VT技术，这个需要在BIOS里面检查。具体的原话就是：

```
Check BIOS No Execute setting
Hyper-V requires hardware support, including: an x64 CPU; VT (Intel) or AMD-V (AMD) hardware extensions; No eXecute (NX)/eXecute Disable (XD) and full BIOS support for hardware virtualization.

Check your physical computer's BIOS settings to ensure that the No Execute BIOS setting is enabled, then turn off the power to your physical computer. Restart the physical computer. NOTE: resetting the physical computer is not sufficient.
```

所以总结就是需要4点: 
1. X64 CPU，这个是自然的；
2. VT技术，不管是intel VT还是AMD-V
3. 数据执行保护， NX或者XD
4. 其他设置，就是各种硬件虚拟化功能都打开吧；

虽然网上也搜到了这么一个图，但是我的华硕BIOS里面竟然只看到了intel VT这个，没发现XD，最后直接load optimised了事；

![hyper-V的BIOS设置](https://cache.yisu.com/upload/information/20200311/45/196788.jpg)



# 软件-安装

## 打开内置的hyper-v功能
这个就是老生常谈了，打开控制面板，找到程序与功能，然后打开hyper-V功能，其他我还喜欢WSL，也是虚拟机 :)  当然需要重启才能看到效果

![程序与功能](http://img1.2345.com/duoteimg/zixunImg/local/2012/04/09/13339604565630.jpg)

## 关闭安全启动，一代虚拟机安装win7
安装windows，这里还有一个坑，就是需要关闭安全启动，否则报错说你的ISO文件hash值不满足要求；此外win7需要一代的虚拟机
![创建虚拟机](http://n.sinaimg.cn/sinacn20190518s/488/w728h560/20190518/ad3d-hwzkfpv0948510.jpg)

## 创建内部虚拟路由器，实现主机和虚拟机网络连接；最主要的是找到主机里的虚拟网卡，把它和虚拟机里的网卡设置成同一个网段，相互ping通

![虚拟路由器-内部](https://exp-picture.cdn.bcebos.com/0d55dc7bd2828689b62dff0265f97fbd4d7c37c2.jpg?x-bce-process=image%2Fresize%2Cm_lfit%2Cw_500%2Climit_1%2Fformat%2Cf_jpg%2Fquality%2Cq_80)

## 主机安装一个filezilla ftp server，为虚拟机提供文件交换能力，这里重要的一点是打开防火墙

![安装ftpserver](https://blog.csdn.net/weixin_46450938/article/details/104661398)

## 下一步安装国版软件，开始干活。

回想一下，为了干点活，踩了这么多坑。


























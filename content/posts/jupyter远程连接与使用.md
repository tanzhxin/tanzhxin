---
title: "Jupyter远程连接与使用"
date: 2020-04-27T16:56:52+08:00
draft: false
---

## 使用conda包管理器或者pip3安装jupyter, pip需要使用aliyun镜像来安装jupyter包

```
pip3 install jupyter-notebook
```
## 生成默认配置文件以便修改
```
jupyter-notebook --generate-config
```

## 在ipython终端下生成远程连接密匙
```
from notebook.auth  import passwd
passwd()
Enter password: 
Verify password: 
Out[2]: 'sha1:1b4ea9662b35:3e3d6a823d264d466f125a0939623c05e7b66007'
```
复制，准备粘贴到配置文件中。

## 修改配置文件~/.jupyter/jupyter_notebook_config.py

修改ip、端口、不自动打开浏览器
```
c.NotebookApp.ip='*'                  # ×允许任何ip访问
c.NotebookApp.open_browser = False
c.NotebookApp.password = 'sha1:xxxx'  # key  
c.NotebookApp.port = 7777             # 可自行指定一个端口, 访问时使用该端口
```

## 测试
在服务器端运行 nohup jupyter-notebook & ，根据提示的ip地址端口打开浏览器连接，输入设置的密匙字符串，通过远程连接认证。

然后就可以创建ipynb文件，实现与matlab notebook类似的文学编程体验。



---
title: "SL7下安装odoo12"
date: 2019-01-29T17:19:14+08:00
draft: false
---


参考 https://linuxize.com/post/install-odoo-12-on-centos-7/

缘起：用户管理需求，测试小型CRM及ERP工具，ZOHO相对来说不自由，于是用云服务器作了一个自己可控的系统

odoo前身是openerp，是挪威一个哥们写的开源ERP系统，对标的是SAP，目前最新版本是odoo12，发展方向是多功能线上管理系统，目标用户是中小型企业，其他就不多说了。


1 安装Python 3.6

SL7的一个麻烦就是python36要先找centos7的SCL源的rpm包，把这个安装上去，后面才能和centos7一样，repo里自带的python36包不全，所以要用SCL源里的rh-python36。

{% codeblock install python36 %}
#yum install rh-python36 git gcc wget nodejs-less libxslt-devel bzip2-devel openldap-devel libjpeg-devel freetype-devel postgresql-devel
{% endcodeblock %}



2 创建系统用户odoo2、用户组以及目录：
{% codeblock 创建用户 %}
#useradd -m -U -r -d /opt/odoo12 -s /bin/bash odoo12
{% endcodeblock %}

3 安装pgsql-10
当然要用最新的pg，只能用从官方的源来安装了。

{% codeblock 添加pgsql官方源，安装和配置pgsql10 %}
#yum install https://download.postgresql.org/pub/repos/yum/10/redhat/rhel-7-x86_64/pgdg-centos10-10-2.noarch.rpm
#yum install postgresql10-server
{% endcodeblock %}

{% codeblock 配置%}
#/usr/pgsql-10/bin/postgresql-10-setup initdb -E UTF-8
#systemctl enable postgresql-10
#systemctl start postgresql-10
#su - postgres -c "createuser -s odoo12"
{% endcodeblock %}

并且创建一个与系统用户同名的数据库用户，方便认证。

4 安装pdf工具包

wkhtmltox软件包是一个开源的命令行工具，它可以将html转换成pdf等其他各种格式
{% codeblock 下载和安装辅助工具软件 %}
#wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.1/wkhtmltox-0.12.1_linux-centos7-amd64.rpm
#yum localinstall wkhtmltox-0.12.1_linux-centos7-amd64.rpm
{% endcodeblock %}


5 下载安装和配置

首先切换到同名系统用户

{% codeblock 切换用户 %}
su - odoo12
{% endcodeblock %}


从官方git源克隆最新odoo12代码

{% codeblock 克隆源%}
git clone https://www.github.com/odoo/odoo --depth 1 --branch 12.0 /opt/odoo12/odoo
{% endcodeblock %}


使用rh-python36

{% codeblock %}
scl enable rh-python36 bash
{% endcodeblock %}

创建用于odoo的新虚拟环境，并激活；

{% codeblock %}
cd /opt/odoo12
python3 -m venv venv
source venv/bin/activate
{% endcodeblock %}

更新pip，并安装odoo12所需模块，确保顺利满足所有依赖要求，退出虚拟环境

{% codeblock %}
pip3 install -r odoo/requirements.txt

deactivate
{% endcodeblock %}


以odoo12用户名建立本地附加模块目录，退出到超级用户

{% codeblock %}
mkdir /opt/odoo12/odoo-custom-addons
exit
{% endcodeblock %}


创建系统配置文件，内容如下：

{% codeblock %}
#nano /etc/odoo12.conf
{% endcodeblock %}

{% codeblock %}

/etc/odoo12.conf
[options]
; This is the password that allows database operations:
admin_passwd = superadmin_passwd
db_host = False
db_port = False
db_user = odoo12
db_password = False
addons_path = /opt/odoo12/odoo/addons, /opt/odoo12/odoo-custom-addons
{% endcodeblock %}


记得修改超级管理密码。



创建systemd 单元文件来负责服务的启动和停止

{% codeblock %}
#nano /etc/systemd/system/odoo12.service
{% endcodeblock %}

/etc/systemd/system/odoo12.service 内容如下

{% codeblock %}
[Unit]
Description=Odoo12
Requires=postgresql-10.service
After=network.target postgresql-10.service

[Service]
Type=simple
SyslogIdentifier=odoo12
PermissionsStartOnly=true
User=odoo12
Group=odoo12
ExecStart=/usr/bin/scl enable rh-python36 -- /opt/odoo12/venv/bin/python3 /opt/odoo12/odoo/odoo-bin -c /etc/odoo12.conf
StandardOutput=journal+console

[Install]
WantedBy=multi-user.target
{% endcodeblock %}

存盘后退出。



更新systemd单元文件并启动odoo服务

{% codeblock 启动服务%}
systemctl daemon-reload
systemctl enable odoo12
systemctl start odoo12
{% endcodeblock %}

检查服务状态，确保正常运行。


{% codeblock %}
#systemctl status odoo12
#journalctl -u odoo12
{% endcodeblock %}


打开页面检查安装结果，如果看不到页面，检查端口8069是否开放，把防火墙的firewalld关闭或者打开端口

{% codeblock %}
http://<your_domain_or_IP_address>:8069

#netstat -lpn

#firewall-cmd --permanent --zone=public --add-port=8069/tcp
#firewall-cmd --reload
{% endcodeblock %}

看到如下页面即告成功。

{% asset_img odoo-12-centos.png  首页 %}









---
title: "Ntpdate Server List"
date: 2019-05-09T11:02:04+08:00
draft: false
---

Name  	IP 	Location
time.nist.gov 	192.43.244.18 	NCAR,Boulder,Colorado
210.72.145.44 	210.72.145.44 	中国（国家授时中心）
133.100.11.8 	133.100.11.8 	日本（福冈大学）
time-a.nist.gov 	129.6.15.28 	NIST,Gaithersburg,Maryland
time-b.nist.gov 	129.6.15.29 	NIST,Gaithersburg,Maryland
time-a.timefreq.bldrdoc.gov 	132.163.4.101 	NIST,Boulder,Colorado
time-b.timefreq.bldrdoc.gov 	132.163.4.102 	NIST,Boulder,Colorado
time-c.timefreq.bldrdoc.gov 	132.163.4.103 	NIST,Boulder,Colorado
utcnist.colorado.edu 	128.138.140.44 	UniversityofColorado,Boulder
time-nw.nist.gov 	131.107.1.10 	Microsoft,Redmond,Washington
nist1.symmetricom.com 	69.25.96.13 	Symmetricom,SanJose,California
nist1-dc.glassey.com 	216.200.93.8 	Abovenet,Virginia
nist1-ny.glassey.com 	208.184.49.9 	Abovenet,NewYorkCity
nist1-sj.glassey.com 	207.126.98.204 	Abovenet,SanJose,California
nist1.aol-ca.truetime.com 	207.200.81.113 	TrueTime,AOLfacility,Sunnyvale,California
nist1.aol-va.truetime.com 	64.236.96.53 	TrueTime,AOLfacility,Virginia



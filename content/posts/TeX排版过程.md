--- 
title: "TeX排版过程"
date: 2021-04-22T21:25:48+08:00
draft: false
---


![排版流程](https://pic2.zhimg.com/80/v2-396d894e8a579348310c948c3757bd98_720w.jpg?source=1940ef5c)

这张图比较好地归纳了TeX 将输入的源代码变成最终输出的排版结果的过程。

第一步，token scanning，暂且译为“记号扫描”，也就是 TeX 将最初读入源代码中的字符转化为“记号”的过程。字母等普通字符是记号，以 \ 开头的命令（或者叫“控制序列”，control sequence）也是记号。

第二步，macro expansion，将 LaTeX 格式的命令、宏包中的命令、用户自定义的命令等展开，直到剩下需要 TeX 直接处理的原始命令（primitives）和字符等记号。在 TeX 中能展开的不仅是宏，一些原始命令也能够展开，如将数字输出为罗马数字格式的原始命令 \romannumeral。

第三步，primitive interpretation，或者叫 primitive processing 也行吧。这一步负责将前两部得到的各种记号转化为排版的“原料”，遇到字符记号则输出字符，遇到构造盒子的命令则输出盒子，遇到一些参数赋值的命令则改变一些状态，等等。每个命令都起到什么作用，在 TeXbook 24-26 章有总结，不过想看懂那一部分，基本上要把 TeXbook 或者 TeX by topic 完整啃一遍。源代码经过前三步的“消化”，生成的排版“原料”以列表的形式组织，根部为一个垂直列表，粗略来讲，包括文字段落、公式、图表盒子等内容；文字段落为一个水平列表；公式则由数学列表的形式组织。在进行下一步之前，数学列表会转化为水平列表，怎么转化的，细节在 TeXbook 附录 G，当然一上来不见得有多少人啃得动。

第四步，line breaking，将水平列表根据给定的宽度断行，生成从上到下排布的一系列盒子。由（不那么）著名的 Knuth-Plass 算法实现。

第五步，page breaking，一页内容从上到下填满，则进行断页操作。这一个步骤可以被利用起来，制造一些特殊的中断，从而实现诸如 LaTeX 处理浮动体之类的操作。

第六步，page building，将前两步生成的页面盒子加上页眉页脚等内容，打包生成最终的页面，所有的字符、图像等排版元素的位置和大小固化在这个页面上。

第七步，将上一个步骤中生成的页面写入输出格式，如 DVI 或 PDF。
